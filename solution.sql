-- customerName na taga Pinas
SELECT customerName FROM customers WHERE country = "Philippines";

-- contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- productName and MSRP of the product named "Titanic"
SELECT productName, MSRP FROM products WHERE productName = "Titanic";

-- firstName and lastName of the employee whose email is jfirrelli@classicmodelcars.com
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- customerName with no registered state
SELECT customerName FROM customers WHERE state IS NULL;

-- firstName, lastName and email of the employee whose lastName is Patterson and firstName is "Steve"
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- customerName, country and creditLimit of customers whose countries are not in USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > "3000";

-- customerName of customers whose customerName dont have a on them
SELECT customerName FROM customers WHERE customerName !="%a%" OR customerName !="%a" OR customerName !="a%";

--customerNumber of orders whose comments contain the string 'DHL'
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%" OR comments LIKE "%DHL" OR comments LIKE "DHL%";

-- productLine with textDescription has "state of the art"
SELECT productLine FROM productlines where textDescription LIKE "%state of the art%"
OR textDescription LIKE "%state of the art"
OR textDescription LIKE "state of the art%";

-- return customer country with no duplicate values
SELECT DISTINCT country FROM customers;

-- status of orders with no duplicate values
SELECT DISTINCT status FROM orders;

-- customerName, country ng customers na may USA, France or Canada
SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

-- firstName, lastName, and office's city of employees whose offices are in Tokyo
SELECT firstName, lastName, city FROM employees 
LEFT JOIN offices.country offices ON employees.officeCode
WHERE offices.country = "Tokyo"

-- employees Leslie Thompson - 1166; customers salesRepEmployeeNumber
-- ibigay customerName
SELECT DISTINCT customerName FROM customers 
JOIN employees 
ON customers.salesRepEmployeeNumber = employees.employeeNumber 
WHERE customers.salesRepEmployeeNumber = "1166" AND employees.employeeNumber = "1166";

-- productName and customerName of products ordered by "Baane Mini Imports"
SELECT products.productName, customers.customerName FROM customers

INNER JOIN products ON productline = products.productLine
JOIN orders ON orderNumber = orders.orderNumber
JOIN orderdetails ON orderlineNumber = orderdetails.orderLineNumber

WHERE customerName = "Baane Mini Imports";

-- employee firstName and lastName customers customerName, offices is display yung bansa na pinag transaksyunan na parehas ang country
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees

JOIN customers ON country = customers.country
JOIN offices
JOIN payments;

-- lastName and firstName of employees supervided by Anthony Bow
SELECT lastName, firstName FROM employees WHERE reportsTo = "1143";

-- productName and MSRP with the highest MSRP
SELECT productName, MAX(MSRP) FROM products;

-- select number of customers in the UK
SELECT * FROM customers WHERE country = "UK";

-- return number of products per product line
SELECT productName, productLine FROM products;

-- return number of customers served by every employee.
SELECT customerName FROM customers WHERE salesRepEmployeeNumber IS NOT NULL;

-- return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.
SELECT productName, quantityInStock FROM products WHERE
productLine = "Planes" AND quantityInStock < 1000; 